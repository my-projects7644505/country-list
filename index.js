document.addEventListener("DOMContentLoaded", function() {
    const countriesContainer = document.getElementById('country-container');

    fetch('https://restcountries.com/v3.1/all')
        .then(response => response.json())
        .then(countries => {
            countries.forEach(country => {
                const countryCard = document.createElement('country-container');
                countryCard.classList.add('country-card');
                const countryFlag = document.createElement('img');
                countryFlag.src = country.flags.png;
                countryFlag.alt = `Flag of ${country.name.common}`;
                countryFlag.classList.add('country-flag');
                const countryName = document.createElement('h2');
                countryName.textContent = country.name.common;
                countryCard.appendChild(countryName);
                countryCard.appendChild(countryFlag);
                countriesContainer.appendChild(countryCard);
             
            });
        })
        .catch(error => console.error('Error fetching countries:', error));
});
